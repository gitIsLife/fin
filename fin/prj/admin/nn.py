from ..settings import settings
import numpy as np
from ..recipe.models import Recipe, Tag, Word, Ingredient
from itertools import chain
from functools import lru_cache


def get_model():
    import keras
    import keras.layers as L

    l_inp_ingredients = L.Input([settings.ingredients_max_tokens, settings.tags_embedding_size], name='ingredients')
    l_conv_ingredients = L.Conv1D(8, 3, activation='relu')(l_inp_ingredients)
    l_pool_ingredients = L.MaxPool1D(2)(l_conv_ingredients)
    l_lstm2_ingredients = L.Bidirectional(L.LSTM(12, activation='sigmoid'))(l_pool_ingredients)

    l_inp_tags = L.Input([settings.tags_max_tokens, settings.tags_embedding_size], name='tags')
    l_conv_tags = L.Conv1D(8, 3, activation='relu')(l_inp_tags)
    l_pool_tags = L.MaxPool1D(2)(l_conv_tags)
    l_lstm2_tags = L.Bidirectional(L.LSTM(12, activation='sigmoid'))(l_pool_tags)

    l_inp_names = L.Input([settings.title_max_tokens, settings.text_embedding_size], name='title')
    l_conv_names = L.Conv1D(8, 3, activation='relu')(l_inp_names)
    l_pool_names = L.MaxPool1D(2)(l_conv_names)
    l_lstm2_names = L.Bidirectional(L.LSTM(6, return_sequences=True))(l_pool_names)
    l_lstm3_names = L.Bidirectional(L.LSTM(12, activation='sigmoid'))(l_lstm2_names)

    l_inp_descriptions = L.Input([settings.description_max_tokens, settings.text_embedding_size], name='description')
    l_conv_description = L.Conv1D(8, 3, activation='relu')(l_inp_descriptions)
    l_pool_description = L.MaxPool1D(3)(l_conv_description)
    l_lstm2_description = L.Bidirectional(L.LSTM(6, return_sequences=True))(l_pool_description)
    l_lstm3_description = L.Bidirectional(L.LSTM(12, activation='sigmoid'))(l_lstm2_description)

    l_inp_nutrition = L.Input([7], name='nutritions')
    l_inp_minutes = L.Input([1], name='minutes')

    l_merged_nm = L.Concatenate()([l_inp_nutrition, l_inp_minutes])
    l_dense_nm = L.Dense(8, activation='relu')(l_merged_nm)
    l_dense2_nm = L.Dense(16, activation='sigmoid')(l_dense_nm)

    l_all_feats = L.Concatenate()([l_lstm2_ingredients, l_lstm2_tags, l_lstm3_names, l_lstm3_description, l_dense2_nm])

    l_dense_all = L.Dense(16, activation='sigmoid')(l_all_feats)
    l_dense2_all = L.Dense(48, activation='sigmoid')(l_dense_all)

    l_out = L.Dense(1)(l_dense2_all)

    model = keras.models.Model(
        inputs=[l_inp_ingredients, l_inp_tags, l_inp_names, l_inp_descriptions, l_inp_nutrition, l_inp_minutes],
        outputs=[l_out]
    )
    model.compile('adam', 'mean_squared_error')

    return model


class EmbeddingsProxy(list):
    def __init__(self, lru_dict, id):
        self.lru_dict = lru_dict
        self.id = id

    @property
    def obj(self):
        if not hasattr(self, '_obj'):
            self.lru_dict.fetch_proxies()
            if not hasattr(self, '_obj'):
                self._obj = self.lru_dict.model.objects.only('embedding').get(id=self.id).embedding
        if hasattr(self, 'id'):
            del self.id
        return self._obj

    def __getitem__(self, item):
        return self.obj[item]

    def __iter__(self):
        return iter(self.obj)

    def __len__(self):
        return self.lru_dict.model.embedding_size


class LRUDict:
    def __init__(self, model):
        self.model = model
        self.proxies = {}

    def fetch_proxies(self):
        objects = {obj.id: obj.embedding for obj in self.model.objects(id__in=list(self.proxies.keys())).only('embedding')}
        for id, proxie in self.proxies.items():
            proxie._obj = objects[id]
        self.proxies = {}

    @lru_cache(100000)
    def __getitem__(self, item):
        proxy = EmbeddingsProxy(self, item)
        self.proxies[item] = proxy
        return proxy


def gen_batch(batch_size, words_dict, tags_dict, ingredients_dict):
    while True:
        recipes = list(Recipe.objects.aggregate({'$sample': {'size': batch_size}}))

        X = {
            'minutes': np.array([[recipe['minutes']] for recipe in recipes]),
            'nutritions': np.array([[recipe['calories'], recipe['fat'], recipe['sugar'], recipe['sodium'],
                                     recipe['protein'], recipe['saturated_fat'], recipe['carbohydrates']]
                                    for recipe in recipes]),
            'title': np.array([[words_dict[word] for word in recipe['proc_title']] for recipe in recipes]),
            'description': np.array([[words_dict[word] for word in recipe['proc_description']] for recipe in recipes]),
            'tags': np.array([[tags_dict[tag] for tag in recipe['proc_tags']] for recipe in recipes]),
            'ingredients': np.array([[ingredients_dict[ingredient] for ingredient in recipe['proc_ingredients']] for recipe in recipes])
        }
        y = np.array([recipe['average_score'] for recipe in recipes])

        yield X, y


def get_feature_model(model):
    import keras
    import keras.layers as L
    inputs = [l.input for l in model.layers if isinstance(l, L.InputLayer)]
    outputs = [l.output for l in model.layers[-4:]]
    return keras.models.Model(inputs, outputs=outputs)
