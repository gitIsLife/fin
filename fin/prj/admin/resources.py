from ..settings import settings
from json import dumps
from .tasks import initialize
from ..tasks import build_user_profile
from ..util import check_initialization
import falcon


class SettingsResource:
    def on_get(self, req, resp, key):
        resp.body = dumps({key: settings[key]})

    def on_post(self, req, resp, key):
        val = req.context['json']['value']
        settings[key] = val
        resp.body = dumps({key: settings[key]})


class TaskResource:
    TASKS = {
        'initialize': initialize,
        'build_user_profile': build_user_profile,
    }

    @falcon.before(check_initialization)
    def on_post(self, req, resp):
        json = req.context['json']
        task_name, args, kwargs = json['task'], json.get('args', []), json.get('kwargs', {})
        task = self.TASKS[task_name]
        task.delay(*args, **kwargs)
