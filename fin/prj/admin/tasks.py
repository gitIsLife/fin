from ..celery import app as celery
from ..settings import settings
from ..recipe.models import Tag, Ingredient, Word, Recipe
from ..tasks import _get_profile, get_projections
from .nn import get_model, get_feature_model, gen_batch, LRUDict
import numpy as np
from functools import lru_cache
from os.path import join
import subprocess
from ..review.models import Review
from ..user.models import User
from itertools import chain
from bson import ObjectId
from pymongo import UpdateOne
from collections import Counter, defaultdict


@celery.task
def initialize():
    settings.initialized = ''

    train_embeddings()
    feature_model, words_dict, tags_dict, ingredients_dict = train_main_model()
    settings.enable_nn = 'true'
    evaluate_main_model(feature_model, words_dict, tags_dict, ingredients_dict)
    w1, w2, user_profiles = train_libfm()
    settings.enable_update_profile = 'true'
    profiles = update_profiles(w1, w2)
    # profiles = np.vstack((profiles, user_profiles))
    planes = find_planes(profiles)
    update_projections(planes)

    settings.initialized = 'true'


def _train_embeddings(dir, model, **kwargs):
    from gensim.models import Word2Vec
    from gensim.models.word2vec import PathLineSentences
    w2v = Word2Vec(PathLineSentences(dir), **kwargs)

    for id in w2v.wv.vocab:
        model._get_collection().update(
            {'_id': ObjectId(id)},
            {'$set': {'embedding': [float(v) for v in w2v.wv[id]]}}
        )


def train_embeddings():
    print('Starting training embeddings')
    _train_embeddings(settings.text_corpus_path, Word,
                      sg=0, window=3, min_count=0, size=settings.text_embedding_size, workers=1)
    _train_embeddings(settings.tags_corpus_path, Tag,
                      sg=1, window=settings.tags_max_tokens, min_count=0, size=settings.tags_embedding_size, workers=1)
    _train_embeddings(settings.ingredients_corpus_path, Ingredient,
                      sg=1, window=settings.ingredients_max_tokens, min_count=0, size=settings.tags_embedding_size, workers=1)


def train_main_model():
    print('Starting training nn')
    model = get_model()
    words_dict = LRUDict(Word)
    tags_dict = LRUDict(Tag)
    ingredients_dict = LRUDict(Ingredient)
    model.fit(gen_batch(1024, words_dict, tags_dict, ingredients_dict),
              steps_per_epoch=Recipe.objects.count() // 1024, epochs=10, workers=1)
    feature_model = get_feature_model(model)
    feature_model.save(settings.model_path)
    return feature_model, words_dict, tags_dict, ingredients_dict


def _evaluate_on_batch(feature_model, words_dict, tags_dict, ingredients_dict, recipes):
    prediction = feature_model.predict({
        'ingredients': np.array([[ingredients_dict[o] for o in recipe['proc_ingredients']] for recipe in recipes]),
        'tags': np.array([[tags_dict[o] for o in recipe['proc_tags']] for recipe in recipes]),
        'title': np.array([[words_dict[o] for o in recipe['proc_title']] for recipe in recipes]),
        'description': np.array([[words_dict[o] for o in recipe['proc_description']] for recipe in recipes]),
        'nutritions': np.array([[recipe['calories'], recipe['fat'], recipe['sugar'], recipe['sodium'], recipe['protein'],
                                 recipe['saturated_fat'], recipe['carbohydrates']] for recipe in recipes]),
        'minutes': np.array([[recipe['minutes']] for recipe in recipes]),
    })
    updates = []
    stacked = np.hstack(prediction[:-1])
    for i, recipe in enumerate(recipes):
        libfm_features = [float(f) for f in stacked[i, :]]
        updates.append(UpdateOne({'_id': recipe['_id']},
                                 {'$set': {'quality': float(prediction[-1][i][0]), 'libfm_features': libfm_features}}))
    Recipe._get_collection().bulk_write(updates)


def evaluate_main_model(feature_model, words_dict, tags_dict, ingredients_dict):
    print('Starting evaluating nn')
    batch_size = 1024
    batch = []
    for recipe in Recipe._get_collection().find():
        batch.append(recipe)
        if len(batch) == batch_size:
            _evaluate_on_batch(feature_model, words_dict, tags_dict, ingredients_dict, batch)
            batch = []
    if batch:
        _evaluate_on_batch(feature_model, words_dict, tags_dict, ingredients_dict, batch)


class FeatureNumerator:
    def __init__(self):
        self.nxt = -1
        self.reverse_mapping = {}

    @lru_cache(maxsize=None)
    def __getitem__(self, feature):
        self.nxt += 1
        self.reverse_mapping[self.nxt] = feature
        return self.nxt


def parse_libfm_model(model_file, features_count, fn):
    with open(model_file, 'r') as model_file:
        model_file.readline()
        w1 = []
        w2 = []
        do_w2 = False
        for line in model_file.readlines():
            if line.startswith('#'):
                do_w2 = True
                continue
            if do_w2:
                w2.append([float(f) for f in line.split()])
            else:
                w1.append(float(line.strip()))

        profiles = []
        for i in range(features_count, len(w1)):
            profile = list(chain([1], w2[i], [w1[i]]))
            profiles.append(profile)
            User._get_collection().update({'_id': ObjectId(fn.reverse_mapping[i])}, {'$set': {'profile': profile}})

        w1, w2, profiles = np.array(w1[:features_count]), np.array(w2[:features_count]), np.array(profiles)
        np.save(settings.w1_file, w1)
        np.save(settings.w2_file, w2)
        np.save(settings.user_profiles_file, profiles)
        settings.profile_dim = 2 + w2.shape[1]
        return w1, w2, profiles


def fill_libfm_train_file(train_file, test_file):
    print('Starting training libfm')

    @lru_cache(1)
    def get_recipe_features(recipe_id):
        recipe = Recipe.objects.only('libfm_features').get(id=recipe_id)
        return recipe.libfm_features

    fn = FeatureNumerator()
    with open(train_file, 'w') as file:
        for review in Review._get_collection().find().sort('recipe'):
            recipe_features = [f'{fn[i]}:{feature}' for i, feature in enumerate(get_recipe_features(review['recipe']))]
            print(
                review['score'],
                *recipe_features,
                f'{fn[str(review["user"])]}:1.0',
                file=file
            )
    with open(test_file, 'w') as file:
        print(0.0, '42:1.0', file=file)

    return recipe_features, fn


def train_libfm():
    train_file = join(settings.libfm_dir, 'train')
    test_file = join(settings.libfm_dir, 'test')
    model_file = join(settings.libfm_dir, 'model')

    recipe_features, fn = fill_libfm_train_file(train_file, test_file)

    print('Running libfm')
    subprocess.call([settings.libfm_executable, '-task', 'r', '-train', train_file, '-test', test_file,
                     '-method', 'sgd', '-regular', '0.01', '-dim', '0,1,16', '-iter', '100', '-learn_rate', '0.0005',
                     '-save_model', model_file])

    print('Parsing libfm model')
    return parse_libfm_model(model_file, len(recipe_features), fn)


def get_next_plane(ortho):
    x = np.random.uniform(-1, 1, (settings.profile_dim, settings.profile_dim))
    x[:ortho.shape[0], :] = ortho
    y = np.random.uniform(-1, 1, settings.profile_dim)
    y[:ortho.shape[0]] = 0
    return np.linalg.solve(x, y)


def _get_projections(profile, i, planes):
    prj = (planes * profile).sum(axis=1) > 0
    projections = 0
    for j in range(i + 1):
        bit = prj[j]
        if bit:
            projections += 1 << j
    return projections


def get_ortho(profiles, i, planes):
    cnt = Counter()
    means = defaultdict(lambda: np.zeros(settings.profile_dim))
    for j in range(profiles.shape[0]):
        prof = profiles[j, :]
        prj = _get_projections(prof, i, planes)
        cnt[prj] += 1
        means[prj] += prof
    ortho = []
    for tpl in cnt.most_common(settings.profile_dim - 1):
        ortho.append(means[tpl[0]] / tpl[1])
    return np.array(ortho)


def find_planes(profiles):
    print('Starting finding planes')
    np.random.seed(41)
    means = profiles.mean(axis=0)

    planes = np.zeros((settings.projections_dim, settings.profile_dim))
    ortho = means.reshape(1, -1)
    for i in range(settings.projections_dim):
        planes[i, :] = get_next_plane(ortho)
        ortho = get_ortho(profiles, i, planes)
    np.save(settings.planes_file, planes)
    return planes


def update_profiles(w1, w2):
    print('Starting updating profiles')
    profiles = []
    for recipe in Recipe._get_collection().find():
        profile = _get_profile(recipe['libfm_features'], w1, w2)
        profiles.append(profile)
        Recipe._get_collection().update({'_id': recipe['_id']}, {'$set': {'profile': profile}})
    profiles = np.array(profiles)
    np.save(settings.profiles_file, profiles)
    return profiles


def update_projections(planes):
    print('Starting updating projections')
    for recipe in Recipe._get_collection().find():
        projections = get_projections(recipe['profile'], planes)
        Recipe._get_collection().update({'_id': recipe['_id']}, {'$set': {'projections': projections}})
    for user in User._get_collection().find():
        projections = get_projections(user['profile'], planes)
        User._get_collection().update({'_id': user['_id']}, {'$set': {'projections': projections}})
