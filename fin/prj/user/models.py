from mongoengine import Document, StringField, FloatField, IntField, ListField, signals
from mongoengine.errors import DoesNotExist
from bson import ObjectId


class User(Document):
    login = StringField(required=True, min_length=1)

    profile = ListField(FloatField())
    projections = IntField(default=0)

    average_score = FloatField(default=0)
    reviews_count = IntField(default=0)

    meta = {
        'indexes': [
            {'fields': ['login'], 'unique': True}
        ]
    }

    @classmethod
    def find_with_identifier(cls, identifier):
        user = None
        if ObjectId.is_valid(identifier):
            try:
                user = cls.objects.get(id=identifier)
            except DoesNotExist:
                pass
        if user is None:
            user = cls.objects.get(login=identifier)
        return user
