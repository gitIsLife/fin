from .models import User
from .schemas import UserSchema

from ..util import load_with_schema


class UserResource:
    def on_get(self, req, resp, identifier):
        resp.body = UserSchema().dumps(User.find_with_identifier(identifier))


class NewUserResource:
    def on_post(self, req, resp):
        user_data = load_with_schema(req, UserSchema)
        user = User(**user_data)
        user.save()
        resp.body = UserSchema().dumps(user)
