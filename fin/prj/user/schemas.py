from ..util import ObjectIdField
from marshmallow import Schema
from marshmallow.fields import String, Float, Int


class UserSchema(Schema):
    id = ObjectIdField(dump_only=True)
    login = String()

    average_score = Float(dump_only=True)
    reviews_count = Int(dump_only=True)
