from mongoengine import Document, StringField, FloatField, IntField, ReferenceField, ListField, \
    EmbeddedDocumentListField, EmbeddedDocument, errors, signals
from ..settings import settings
from ..user.models import User


class WithEmbeds:
    title = StringField(required=True)
    embedding = ListField(FloatField(), required=True)

    @classmethod
    def insert_new_tokens(cls, new_tokens):
        result = {}
        if new_tokens:
            try:
                unk = cls.objects(title=settings.unk_token).only('embedding').get()
            except errors.DoesNotExist:
                unk = cls(title=settings.unk_token, embedding=[0.0 for _ in range(cls.embedding_size)])
                try:
                    unk.save()
                except errors.NotUniqueError:
                    unk = cls.objects(title=settings.unk_token).only('embedding').get()
            for new_token in set(new_tokens):
                obj = cls(title=new_token, embedding=unk.embedding)
                try:
                    obj.save()
                except errors.NotUniqueError:
                    obj = cls.objects.get(title=new_token)
                result[new_token] = obj
        return result

    @classmethod
    def process_tokens(cls, tokens):
        token_mapping = {
            obj.title: obj
            for obj in cls.objects(title__in=tokens)
        }
        new_tokens = [token for token in tokens if token not in token_mapping]
        token_mapping.update(cls.insert_new_tokens(new_tokens))
        return [token_mapping[token] for token in tokens]


class Tag(Document, WithEmbeds):
    embedding_size = settings.tags_embedding_size
    meta = {
        'indexes': [
            {'fields': ['title'], 'unique': True}
        ]
    }


class RecipeStep(EmbeddedDocument):
    description = StringField(required=True)


class Ingredient(Document, WithEmbeds):
    embedding_size = settings.tags_embedding_size
    meta = {
        'indexes': [
            {'fields': ['title'], 'unique': True}
        ]
    }


class Word(Document, WithEmbeds):
    embedding_size = settings.text_embedding_size
    meta = {
        'indexes': [
            {'fields': ['title'], 'unique': True}
        ]
    }


class Recipe(Document):
    title = StringField(required=True)
    proc_title = ListField(ReferenceField(Word))
    description = StringField(required=True)
    proc_description = ListField(ReferenceField(Word))

    calories = FloatField(min_value=0, required=True)
    fat = FloatField(min_value=0, required=True)
    sugar = FloatField(min_value=0, required=True)
    sodium = FloatField(min_value=0, required=True)
    protein = FloatField(min_value=0, required=True)
    saturated_fat = FloatField(min_value=0, required=True)
    carbohydrates = FloatField(min_value=0, required=True)

    minutes = IntField(min_value=0, required=True)

    owner = ReferenceField(User, required=True)

    steps = EmbeddedDocumentListField(RecipeStep)

    tags = ListField(StringField())
    proc_tags = ListField(ReferenceField(Tag))
    ingredients = ListField(StringField())
    proc_ingredients = ListField(ReferenceField(Ingredient))

    average_score = FloatField(default=0)
    reviews_count = IntField(default=0)

    quality = FloatField(default=0)
    libfm_features = ListField(FloatField())

    profile = ListField(FloatField())
    projections = IntField(default=0)

    meta = {
        'indexes': [
            'owner',
            'quality',
            'average_score',
            'reviews_count',
            {'fields': ['projections', 'pk']}
        ]
    }


def run_processing(sender, document, created=False):
    from ..tasks import process_new_recipe
    if created or {'tags', 'ingredients', 'title', 'description'}.intersection(document._get_changed_fields()):
        process_new_recipe.delay(str(document.id))


signals.post_save.connect(run_processing, sender=Recipe)
