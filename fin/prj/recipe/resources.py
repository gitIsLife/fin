from .models import Recipe
from .schemas import RecipeSchema

from ..util import load_with_schema

# sudo docker kill $(sudo docker ps -q) ; sudo rm -f /var/lib/docker/volumes/fin-libfm/_data/settings ; sudo docker build -t fin . ; sudo docker run -P -i --mount type=volume,src=fin-mongo,dst=/data --mount type=volume,src=fin-corpus,dst=/corpus --mount type=volume,src=fin-libfm,dst=/libfm_data fin
# sudo docker kill $(sudo docker ps -q) ; sudo docker build -t fin . ; sudo docker run -P -i --mount type=volume,src=fin-mongo,dst=/data --mount type=volume,src=fin-corpus,dst=/corpus --mount type=volume,src=fin-libfm,dst=/libfm_data fin

# sudo docker kill $(sudo docker ps -q) ; sudo docker container prune -f ; sudo docker volume prune -f ; sudo docker volume create fin-mongo ; sudo mkdir /var/lib/docker/volumes/fin-mongo/_data/db ; sudo docker volume create fin-corpus ; sudo docker volume create fin-libfm

# sudo docker run -P -i --mount type=volume,src=fin-mongo,dst=/data --mount type=volume,src=fin-corpus,dst=/corpus --mount type=volume,src=fin-libfm,dst=/libfm_data fin
# sudo docker build -t fin .

# sudo docker exec -it $(sudo docker ps -q) mongo fin
# db.recipe.aggregate({$group: {_id: '$projections', cnt: {$sum: 1}}}, {$group: {_id: 0, cnt: {$sum: 1}, max: {$max: '$cnt'}}})
# sudo docker exec -it $(sudo docker ps -q) /bin/sh
# sudo docker exec -it $(sudo docker ps -q) cat project/celery_log


class RecipeResource:
    def on_get(self, req, resp, id):
        resp.body = RecipeSchema().dumps(Recipe.objects.get(id=id))

    def on_put(self, req, resp, id):
        Recipe.objects.get(id=id)
        recipe_data = load_with_schema(req, RecipeSchema)
        recipe = Recipe(**recipe_data, _created=False)
        recipe.id = id
        recipe.save()
        resp.body = RecipeSchema().dumps(recipe)


class NewRecipeResource:
    def on_post(self, req, resp):
        recipe_data = load_with_schema(req, RecipeSchema)
        recipe = Recipe(**recipe_data)
        recipe.save()
        resp.body = RecipeSchema().dumps(recipe)
