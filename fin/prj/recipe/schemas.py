from marshmallow import Schema
from marshmallow.fields import String, Float, Int, Nested, List

from ..util import ObjectIdField


class RecipeStepSchema(Schema):
    description = String(required=True)


class RecipeSchema(Schema):
    id = ObjectIdField(dump_only=True)

    title = String(required=True)
    description = String(missing='nodescription')

    calories = Float(required=True)
    fat = Float(required=True)
    sugar = Float(required=True)
    sodium = Float(required=True)
    protein = Float(required=True)
    saturated_fat = Float(required=True)
    carbohydrates = Float(required=True)

    minutes = Int(required=True)
    owner = ObjectIdField(required=True)
    steps = Nested(RecipeStepSchema, many=True)

    tags = List(String(), required=True)
    ingredients = List(String(), required=True)

    average_score = Float(dump_only=True)
    reviews_count = Int(dump_only=True)
