from .models import Review
from .schemas import ReviewSchema

from ..util import load_with_schema


class NewReviewResource:
    def on_post(self, req, resp):
        review_data = load_with_schema(req, ReviewSchema)
        review = Review(**review_data)
        review.save()
        resp.body = ReviewSchema().dumps(review)
