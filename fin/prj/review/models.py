from mongoengine import Document, StringField, FloatField, ReferenceField, signals
from ..user.models import User
from ..recipe.models import Recipe


class Review(Document):
    recipe = ReferenceField(Recipe, required=True)
    user = ReferenceField(User, required=True)

    score = FloatField(required=True)
    description = StringField()

    meta = {
        'indexes': [
            'recipe',
            'user',
        ]
    }


def process_previews(sender, document, created=False):
    if created:
        recipe = document.recipe
        recipe.reviews_count += 1
        recipe.average_score = (recipe.average_score * (recipe.reviews_count - 1) + document.score) / recipe.reviews_count
        recipe.save()

        user = document.user
        user.reviews_count += 1
        user.average_score = (user.average_score * (user.reviews_count - 1) + document.score) / user.reviews_count
        user.save()


signals.post_save.connect(process_previews, sender=Review)
