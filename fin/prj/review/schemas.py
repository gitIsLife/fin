from marshmallow import Schema
from marshmallow.fields import String, Float

from ..util import ObjectIdField


class ReviewSchema(Schema):
    id = ObjectIdField()

    recipe = ObjectIdField(required=True)
    user = ObjectIdField(required=True)

    score = Float(required=True)
    description = String()
