from marshmallow.fields import String
from marshmallow.schema import ValidationError
from bson import ObjectId
from bson.errors import InvalidId
import json
import falcon
from .settings import settings


def load_with_schema(req, schema_cls, many=False):
    schema_inst = schema_cls(many=many)
    req.context['document'] = schema_inst.load(req.context['json'])
    return req.context['document']


class ObjectIdField(String):
    def _serialize(self, value, attr, obj, **kwargs):
        if isinstance(value, ObjectId):
            value = str(value)
        return super(ObjectIdField, self)._serialize(value, attr, obj, **kwargs)

    def _deserialize(self, value, attr, data, **kwargs):
        string = super(ObjectIdField, self)._deserialize(value, attr, data, **kwargs)
        try:
            return ObjectId(string)
        except (InvalidId, TypeError):
            raise ValidationError('Invalid ObjectId')


def check_initialization(req, resp, resource, params):
    if not settings.initialized:
        raise falcon.HTTPBadRequest('Action is unavailable until initialization')
