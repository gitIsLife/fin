from os import environ, path
from functools import lru_cache
from json import load, dump


class SettingsFromEnv:
    def __init__(self, mapping):
        super(SettingsFromEnv, self).__setattr__('mapping', mapping)

    @lru_cache(None)
    def __read_env_cached(self, item, default, t):
        return t(environ.get(item, default))

    @lru_cache(None)
    def __get_item_config(self, item):
        m = self.mapping[item]
        default = None
        load_from_env = True
        if isinstance(m, type):
            t = m
        elif len(m) == 1:
            t = m[0]
        elif len(m) == 2:
            t, default = m
        else:
            t, default, load_from_env = m
        return t, default, load_from_env

    def __load_settings_file(self):
        settings_file = environ.get('SETTINGS_FILE', '/libfm_data/settings')
        if path.exists(settings_file):
            with open(settings_file, 'r') as file:
                return load(file)
        return {}

    def __getitem__(self, item):
        t, default, load_from_env = self.__get_item_config(item)
        if load_from_env:
            return self.__read_env_cached(item.upper(), default, t)
        return t(self.__load_settings_file().get(item, default))

    def __setitem__(self, item, value):
        t, default, load_from_env = self.__get_item_config(item)
        assert not load_from_env, 'Must change only file settings'
        new_config = self.__load_settings_file()
        new_config[str(item)] = str(value)
        settings_file = environ.get('SETTINGS_FILE', '/libfm_data/settings')
        with open(settings_file, 'w') as file:
            return dump(new_config, file)

    def __getattr__(self, item):
        try:
            return self[item]
        except:
            raise AttributeError(item)

    def __setattr__(self, key, value):
        self[key] = value


settings = SettingsFromEnv({
    'title_max_tokens': (int, 16),
    'description_max_tokens': (int, 250),
    'ingredients_max_tokens': (int, 30),
    'tags_max_tokens': (int, 100),

    'tags_embedding_size': (int, 8),
    'text_embedding_size': (int, 16),

    'pad_token': (str, '__EOS__'),
    'trunc_token': (str, '__TRUNC__'),
    'unk_token': (str, '__UNK__'),

    'save_corpus': (str, 'true', False),
    'unk_corpus': (float, 0.0025, False),
    'unk_corpus_chance': (float, 0.15, False),

    'tags_corpus_path': (str, '/corpus/tags'),
    'ingredients_corpus_path': (str, '/corpus/ingredients'),
    'text_corpus_path': (str, '/corpus/text'),

    'model_path': (str, '/libfm_data/nn_model.hdf5', False),
    'enable_nn': (str, '', False),
    'enable_update_profile': (str, '', False),
    'initialized': (str, 'true', False),

    'libfm_executable': (str, '/libfm/libFM'),
    'libfm_dir': (str, '/libfm_data'),
    'w1_file': (str, '/libfm_data/w1.npy'),
    'w2_file': (str, '/libfm_data/w2.npy'),
    'planes_file': (str, '/libfm_data/planes.npy'),
    'profiles_file': (str, '/libfm_data/profiles.npy'),
    'user_profiles_file': (str, '/libfm_data/user_profiles.npy'),

    'projections_dim': (int, 12, False),
    'profile_dim': (int, 18, False)
})
