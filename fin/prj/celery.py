from celery import Celery
from celery.signals import worker_process_init

app = Celery(
    'prj',
    broker='redis://localhost:7456/0',
    include=['prj.tasks', 'prj.admin.tasks'],
)

app.conf.update(
    result_expires=3600,
)


@worker_process_init.connect
def connect_to_mongo(sender=None, conf=None, **kwargs):
    from mongoengine import connect
    connect('fin')
