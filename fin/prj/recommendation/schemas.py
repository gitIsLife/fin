from marshmallow import Schema
from marshmallow.fields import Float, Nested, Dict, String

from ..recipe.schemas import RecipeSchema


class RecommendationsSchema(Schema):
    data = Nested(RecipeSchema, many=True)
    next = Dict()
