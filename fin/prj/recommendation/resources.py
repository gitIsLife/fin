from ..recipe.models import Recipe
from .schemas import RecommendationsSchema
from ..user.models import User
from ..settings import settings

from falcon import HTTPBadRequest
from bson import ObjectId


class CommonRecommendationResource:
    def on_get(self, req, resp):
        field_param = int(req.params.get('use_average_score', 0))
        field = 'average_score' if field_param else 'quality'
        page_size = int(req.params.get('page_size', 10))
        page_number = int(req.params.get('page', 1))
        min_reviews = int(req.params.get('min_reviews', 10))
        qs = Recipe.objects()
        if min_reviews:
            qs = qs.filter(reviews_count__gte=min_reviews)

        recipes = (qs.order_by('-' + field)[(page_number - 1) * page_size: page_number * page_size])

        res = {
            'data': recipes,
            'next': {
                'page': page_number + 1,
                'page_size': page_size,
                'min_reviews': min_reviews,
                'use_average_score': field_param
            }
        }

        resp.body = RecommendationsSchema().dumps(res)


def get_next_combination(num, max_bits):
    ones_count = 0
    for bit_num in range(max_bits):
        bit = num & (1 << bit_num)
        if bit:
            ones_count += 1
        else:
            if ones_count:
                return ((1 << (ones_count - 1)) - 1) + (1 << bit_num) + ((num >> bit_num) << bit_num)
    return (1 << (ones_count + 1)) - 1


class PersonalizedRecommendationResource:
    def on_get(self, req, resp, identifier):
        xor = int(req.params.get('xor', 0))
        page_size = int(req.params.get('page_size', 10))
        last_id = ObjectId(req.params.get('last_id', '0' * 24))

        user = User.find_with_identifier(identifier)
        if not user.projections:
            raise HTTPBadRequest(title='User is not initialized')
        recipes = list(Recipe.objects(projections=user.projections ^ xor, id__gt=last_id).order_by('id')[:page_size])

        if len(recipes) < page_size:
            xor = get_next_combination(xor, settings.projections_dim)
            last_id = '0' * 24
        else:
            last_id = str(recipes[-1].id)

        res = {
            'data': recipes,
            'next': {
                'last_id': last_id,
                'page_size': page_size,
                'xor': xor,
            }
        }

        resp.body = RecommendationsSchema().dumps(res)
