from .celery import app as celery
from .recipe.models import Recipe, Word, Tag, Ingredient
from .review.models import Review
from .user.models import User
from nltk.tokenize import RegexpTokenizer, WordPunctTokenizer
from nltk.corpus import stopwords
from os.path import join, exists
from os import makedirs, getpid
import numpy as np
from numpy.random import uniform
from .settings import settings
from itertools import chain


def save_tokens_as_text(tokens, directory):
    if settings.save_corpus:
        makedirs(directory, exist_ok=True)
        path = join(directory, f'{getpid()}')
        mode = 'a' if exists(path) else 'w'

        with open(path, mode) as file:
            print(*[str(token.id) for token in tokens], file=file)

            if settings.unk_corpus and uniform() < settings.unk_corpus:
                unk_token = type(tokens[0]).objects.only('id').get(title=settings.unk_token).id
                tokens = [str(unk_token if uniform() < settings.unk_corpus_chance else token.id) for token in tokens]
                print(*tokens, file=file)


def _fit_size(size, tokens, stop_words):
    pad_token = settings.pad_token
    arr = [pad_token] * size
    trunc_token = settings.trunc_token
    filled = 0
    fit_size = len(arr)
    for token in tokens:
        token = token.lower()
        if token not in stop_words:
            if filled == fit_size:
                arr[-1] = trunc_token
                return arr
            arr[filled] = token
            filled += 1
    return arr


def tokenize_sent(sent, tokenizer, stop_words, fit_size):
    return _fit_size(fit_size, tokenizer.tokenize(sent), stop_words)


def process_text_fields(recipe: Recipe):
    tokenizer = RegexpTokenizer(r'[\w-]+')
    stop_words = set(stopwords.words('english'))

    tokenized_title = tokenize_sent(recipe.title, tokenizer, stop_words, settings.title_max_tokens)
    recipe.proc_title = Word.process_tokens(tokenized_title)

    tokenized_description = tokenize_sent(recipe.description, tokenizer, stop_words, settings.description_max_tokens)
    recipe.proc_description = Word.process_tokens(tokenized_description)

    tokenized_tags = _fit_size(settings.tags_max_tokens, recipe.tags, set())
    recipe.proc_tags = Tag.process_tokens(tokenized_tags)

    tokenized_ingredients = _fit_size(settings.ingredients_max_tokens, recipe.ingredients, set())
    recipe.proc_ingredients = Ingredient.process_tokens(tokenized_ingredients)


def evaluate_model(recipe, model):
    prediction = model.predict({
        'ingredients': np.array([[o.embedding for o in recipe.proc_ingredients]]),
        'tags': np.array([[o.embedding for o in recipe.proc_tags]]),
        'title': np.array([[o.embedding for o in recipe.proc_title]]),
        'description': np.array([[o.embedding for o in recipe.proc_description]]),
        'nutritions': np.array([[recipe.calories, recipe.fat, recipe.sugar, recipe.sodium, recipe.protein,
                                 recipe.saturated_fat, recipe.carbohydrates]]),
        'minutes': np.array([[recipe.minutes]]),
    })

    recipe.quality = float(prediction[-1][0][0])
    recipe.libfm_features = [float(f) for f in np.hstack(prediction[:-1])[0, :]]

    recipe.save()


@celery.task
def predict_nn(id):
    if not settings.enable_nn:
        return
    import keras
    model = keras.models.load_model(settings.model_path)
    recipe = Recipe.objects.get(id=id)
    evaluate_model(recipe, model)
    update_profile.delay(id)


@celery.task
def process_new_recipe(id):
    recipe = Recipe.objects.get(id=id)

    process_text_fields(recipe)
    recipe.save()

    save_tokens_as_text(recipe.proc_tags, settings.tags_corpus_path)
    save_tokens_as_text(recipe.proc_ingredients, settings.ingredients_corpus_path)
    save_tokens_as_text(recipe.proc_title, settings.text_corpus_path)
    save_tokens_as_text(recipe.proc_description, settings.text_corpus_path)

    predict_nn.delay(id)


def _get_profile(libfm_features, w1, w2):
    first = 0
    middle = np.zeros(w2.shape[1])
    for i, v in enumerate(libfm_features):
        first += v * w1[i]
        for j in range(i, w2.shape[1]):
            first += (w2[i, :] * w2[j, :] * v * libfm_features[j]).sum()
            middle += w2[i, :] * v

    return [
        float(val)
        for val in chain([first] + list(middle) + [1])
    ]


@celery.task
def update_profile(id):
    if not settings.enable_update_profile:
        return
    recipe = Recipe.objects.get(id=id)

    w1 = np.load(settings.w1_file)
    w2 = np.load(settings.w2_file)

    recipe.profile = _get_profile(recipe.libfm_features, w1, w2)
    recipe.projections = get_projections(recipe.profile)

    recipe.save()


def get_projections(profile, planes=None):
    if planes is None:
        planes = np.load(settings.planes_file)

    profile = np.array(profile)
    prj = (planes * profile).sum(axis=1) > 0

    projections = 0
    for i, bit in enumerate(prj):
        if bit:
            projections += 1 << i
    return projections


def step_user_profile(user, review, lr):
    recipe_profile = np.array(review.recipe.profile)

    apprx = (user.profile * recipe_profile).sum()
    err = review.score - apprx

    user.profile = (recipe_profile * err * lr) + user.profile


@celery.task
def build_user_profile(user_id, epochs=5, lr=0.01):
    user = User.objects.get(id=user_id)
    reviews = list(Review.objects(user=user))

    if not user.profile:
        user.profile = np.random.normal(0, 0.1, settings.profile_dim)
    user.profile = np.array(user.profile)

    for i in range(epochs):
        np.random.shuffle(reviews)
        for review in reviews:
            step_user_profile(user, review, lr)

    user.profile = [float(f) for f in user.profile]
    user.projections = get_projections(user.profile)

    user.save()
