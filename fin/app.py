import falcon
from mongoengine.errors import NotUniqueError, DoesNotExist, ValidationError
from mongoengine import connect
from prj.recipe.resources import RecipeResource, NewRecipeResource
from prj.user.resources import UserResource, NewUserResource
from prj.review.resources import NewReviewResource
from prj.admin.resources import SettingsResource, TaskResource
from prj.recommendation.resources import CommonRecommendationResource, PersonalizedRecommendationResource


class MongoConnectionMiddleware:
    def process_request(self, req, resp):
        connect('fin')


class JsonMiddleware:
    def process_request(self, req, resp):
        if req.method in {'PUT', 'POST'}:
            import json
            try:
                req.context['json'] = json.load(req.stream, encoding='utf-8')
            except:
                raise falcon.HTTPBadRequest('Only json is accepted')


app = falcon.API(middleware=[JsonMiddleware(), MongoConnectionMiddleware()])

app.add_route('/recipe/{id}', RecipeResource())
app.add_route('/recipe', NewRecipeResource())
app.add_route('/user/{identifier}', UserResource())
app.add_route('/user', NewUserResource())
app.add_route('/review', NewReviewResource())

app.add_route('/top', CommonRecommendationResource())
app.add_route('/user/{identifier}/recommend', PersonalizedRecommendationResource())

app.add_route('/admin/settings/{key}', SettingsResource())
app.add_route('/admin/task', TaskResource())


def default_error_handler(ex, req, resp, params):
    if isinstance(ex, falcon.http_error.HTTPError):
        raise ex
    raise falcon.HTTPInternalServerError(title='Backend is broken')


def mongo_not_unique_error_handler(ex, req, resp, params):
    raise falcon.HTTPConflict(title='Conflict')


def mongo_not_found_error_handler(ex, req, resp, params):
    raise falcon.HTTPNotFound(title='Not found')


def validation_error_handler(ex, req, resp, params):
    raise falcon.HTTPBadRequest(title='Validation failed')


app.add_error_handler(Exception, default_error_handler)
app.add_error_handler(NotUniqueError, mongo_not_unique_error_handler)
app.add_error_handler(DoesNotExist, mongo_not_found_error_handler)
app.add_error_handler(ValidationError, validation_error_handler)
