FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y wget gnupg
RUN wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add -
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list
RUN apt-get update
RUN apt-get install -y mongodb-org

RUN apt-get install -y python3 python3-pip
RUN pip3 install ipython
ENV PRJ=/project
COPY fin/requirements.txt /project/requirements.txt
RUN pip3 install -r /project/requirements.txt
RUN python3 -c 'import nltk; nltk.download("stopwords")'

RUN pip3 install "celery[redis]"
COPY redis-6.0.1/src /redis

EXPOSE 8000

COPY libfm /libfm
COPY fin /project

CMD mongod --dbpath /data/db --logpath /data/db/log --logappend --fork && /redis/redis-server --port 7456 --daemonize yes && cd $PRJ && celery -A prj worker -l debug -f celery_log -D --concurrency=2 && uwsgi $PRJ/uwsgi.ini
